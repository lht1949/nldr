
# coding: utf-8

# In[1]:

# ------------------------------
# Import Python libraries
# -----------------------------
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from sklearn.decomposition import PCA


# In[2]:

get_ipython().magic('matplotlib inline')


# # Load the files from the directory

# In[3]:

source = pd.read_csv("data-returns.csv") # C:/WPI/PhD/09152016/
tickers = pd.read_table("tickers.txt") # C:/WPI/PhD/09152016/
frame = pd.DataFrame(source)


# # PCA 

# In[4]:

pca = PCA().fit(frame)
plt.figure(figsize=(7,7)) 
plt.plot(np.cumsum(pca.explained_variance_ratio_))
plt.xlabel('number of components')
plt.ylabel('cumulative explained variance')
plt.title('PCA')
plt.show()


# # Singular Value Decomposition 

# In[88]:

from numpy.linalg import svd
U, s, V = svd(frame, full_matrices=True)

Y = U * np.sqrt(s)
Y_svd = Y[:,0:2]


# In[6]:

# Reconstruction based on full SVD
S = np.zeros((U.shape[0], V.shape[0]), dtype=complex)
S[:s.shape[0], :s.shape[0]] = np.diag(s)

#np.allclose(frame, np.dot(U, np.dot(S, V)))

new_frame = np.dot(U, np.dot(S, V))

plt.figure(figsize=(20,10)) 
plt.plot(new_frame.T)
plt.title('S&P 500 - Intraday Returns')
plt.show()


# # Reconstruction based on reduced SVD

# In[7]:

U, s_svd, V = np.linalg.svd(frame, full_matrices=False)
plt.figure(figsize=(20,10))
plt.title('DR using SVD')
plt.plot(s_svd)


# This looks rank one to me and the plot confirms it.

# In[9]:

plt.figure(figsize=(20,10))
plt.imshow(frame,vmin=-0.02,vmax=0.02)
plt.colorbar()


# # Plot the frame after standardizing

# In[10]:

Yn = (frame -frame.mean())/frame.std()


# In[11]:

plt.figure(figsize=(20,10))
plt.title('Intraday Returns after Standardizing Data')
plt.imshow(Yn,vmin=-0.02,vmax=0.02)
plt.colorbar()


# # Calculate correlation between instruments and correlation between time

# In[12]:

Y = frame
Minstrument = Y.T.corr()
Mtime = Y.corr()


# In[13]:

plt.figure(figsize=(20,10))
plt.imshow(Minstrument)
plt.colorbar()
plt.title('Correlation between instruments')


# In[14]:

plt.figure(figsize=(20,10))
plt.imshow(Mtime)
plt.title('Correlation between time')
plt.colorbar()


# # Multidimensional Scaling (MDS)

# In[61]:

# ---------------------------------------------
# Multidimenional Scaling Implementation 
# ---------------------------------------------
from scipy.spatial.distance import pdist
from numpy.linalg import norm

def get_distance_matrix(df):
    rows = len(df)
    distance = np.zeros((rows, rows))
    
    for i, rowi in df.iterrows():
        for j, rowj in df.iterrows():
            distance[i,j] = norm(rowi - rowj)
            
    return distance

def mds(d, dimensions=2):
    """
    Refer to page 77 of NLDR Eq (4.49) for algorithm details
    """
    (n,n) = d.shape
    E = (-0.5 * d**2)
    
    # row and column means
    Er = np.mat(np.mean(E,1))
    Es = np.mat(np.mean(E,0))
    
    F = np.array(E - np.transpose(Er) - Es + np.mean(E))
    
    [U, S, V] = svd(F)
    
    Y = U * np.sqrt(S)

    return (Y[:,0:dimensions], S, U, V)


# In[62]:

distance = get_distance_matrix(frame)  ### this is a slow operation


# In[63]:

[Y_mds, S_mds, U_mds, V_mds] = mds(distance)
Yr = Y_mds[:,0:2]


# In[64]:

# Two subplots, unpack the axes array immediately
f, (ax1, ax2) = plt.subplots(1, 2, sharey=True, figsize=(20,10))
ax1.plot(s_svd)
ax1.set_title('Singular Value Decomposition')

ax2.set_title('Mutlidimensional Scaling')
ax2.plot(S_mds)


# # ISOMAP

# In[67]:

import math

def list_components(adj):
    # Now rows may be duplicate, create g x n matrix where g is number
    # of unique groups and n is number of sensors
    groups = {}

    for row in adj:
        groups[str(row)] = row

    return [np.nonzero(row)[0] for row in groups.values()]

def flatidx(idx):
    adds = np.arange(0, len(idx) ** 2, len(idx))
    return np.ravel(np.transpose(idx) + adds)

def slice_matrix(m,i,j):
    """
    i is an array of row indices
    j is an array of column indices
    m is a matrix where we want to slice out rows i and cols j
    """
    return np.take(np.take(m,i,0),j,1)


def cluster_graph(d, fnc = 'k', size = 7, graph = 'adjacency'):
    """
    An essential part of isomap is the construction of a graph. We
    can use this graph for other kinds of structural analysis.
    """

    # TODO: do sparse clustering

    # the put operation is destructive to d
    ld = d.copy()

    if fnc == 'k':
        if not type(size) == int:
            raise(TypeError, "Size must be an integer")
        else:
            # sort to find initial k clusters
            idx = np.argsort(ld)

            # transform row indices for square d to row indices for flat d
            np.put(ld, flatidx(idx[:,size:]), math.inf)

    elif fnc == 'epsilon':
        if not type(size) == float:
            raise( TypeError, "Size must be a float")
        else:
            ld = np.where(np.less(ld, size), ld, math.inf)
    else:
        raise(ValueError, "Unknown fnc type")

    # ensure that the result is symmetric
    ld = np.minimum(ld, np.transpose(ld))

    if graph == 'adjacency':
        return np.less(ld, math.inf) # boolean matrix. use astype(int) for 0-1 matrix.
    elif graph == 'distance':
        """ Return the shortest paths. """
        return shortest_paths(ld)
    else:
        raise(ValueError, "Unknown graph type!")

def shortest_paths(adj, alg = "Floyd1"):
    (n,m) = adj.shape
    assert n == m
    if alg == "Floyd1":
        for k in range(n):
            adj = np.minimum( adj, np.add.outer(adj[:,k],adj[k,:]) )
        return adj
    elif alg == "Floyd2": # variant taken from original Isomap implementation
        for k in range(n):
            adj = np.minimum(adj, np.tile(adj[:,k].reshape(-1,1),(1,n)) + np.tile(adj[k,:],(n,1)))
        return adj
    else:
        raise(Error, "Not implemented")

        
def isomap(d, fnc = 'k', size = 7, dimensions = 2):
    """ Compute isomap instead of mds. Currently neighborhoods of type k are supported. """
    if not len(d.shape) == 2:
        raise ValueError("d must be a square matrix")      
              

    # the put operation is destructive to d
    ld = cluster_graph(d, fnc = fnc, size = size, graph = 'distance')
    adj = cluster_graph(d, fnc = fnc, size = size, graph = 'adjacency')

    # shortest paths will find connected components
    tmp = np.less(ld, math.inf) # 0-1
    groups = list_components(tmp)

    # now do classical mds on largest connected component
    groups.sort(key=lambda x: len(x), reverse=True)

    dg = slice_matrix(ld, groups[0], groups[0])

    Y,eigs, U, V = mds(dg, dimensions)

    return (Y, eigs, adj, U, V)

def norm(vec):
    return np.sqrt(np.sum(vec**2))

def square_points(size):
    nsensors = size ** 2
    return np.array([(i / size, i % size) for i in range(nsensors)])


def test():
    [Y, eigs, adj, U, V] = isomap(distance)
    return [Y, eigs, adj, U, V]


# In[68]:

[Y_isomap, S_isomap, adj, U_isomap, V_isomap] = test()

# Two subplots, unpack the axes array immediately
f, (ax1, ax2) = plt.subplots(1, 2, sharey=True, figsize=(20,10))
ax1.set_title('Mutlidimensional Scaling')
ax1.plot(S_mds)

ax2.set_title('Isomap')
ax2.plot(S_isomap)

plt.show()


# In[101]:

fig = plt.figure(figsize=(20,10))

ax1 = fig.add_subplot(131)
ax1.plot(s_svd)
ax1.set_title('Singular Value Decomposition')
plt.xlabel('# of instruments')
plt.ylabel('Eigen values')

ax2 = fig.add_subplot(132)
ax2.plot(S_mds)
ax2.set_title('Mutlidimensional Scaling')
plt.xlabel('# of instruments')
plt.ylabel('Eigen values')

ax3 = fig.add_subplot(133)
ax3.plot(S_isomap)
ax3.set_title('Isomap')
plt.xlabel('# of instruments')
plt.ylabel('Eigen values')


# In[96]:

# ----------------------------------------------------
# let us plot the anomalous returns in lower dimension
# using all 3 techniques
# -----------------------------------------------------

# fig = plt.figure(figsize=(10,10))

# plt.plot(S_isomap, color='m', label='Isomap')
# plt.plot(s, color='r', label='SVD')
# plt.plot(S_mds='g', label='MDS')


# plt.title('Reconsturction using SVD / MDS / Isomap')
# plt.legend()

# plt.show()


# In[91]:

# ----------------------------------------------------
# let us plot the anomalous returns in lower dimension
# using all 3 techniques
# -----------------------------------------------------

fig = plt.figure(figsize=(10,10))

plt.plot(Y_isomap[:,0],Y_isomap[:,1],'o', markersize=12, color='m', label='Isomap')
plt.plot(Y_svd[:,0],Y_svd[:,1],'o', markersize=12, color='r', label='SVD')
plt.plot(Y_mds[:,0],Y_mds[:,1],'o', markersize=12, color='g', label='MDS')


plt.title('Reconsturction using SVD / MDS / Isomap')
plt.legend()

plt.show()


# In[102]:

# ----------------------------------------------------
# let us plot the anomalous returns in lower dimension
# using all 3 techniques
# -----------------------------------------------------
X_svd = U*s
X_mds = U_mds * S_mds
X_isomap = U_isomap * S_isomap

fig = plt.figure(figsize=(10,10))

plt.plot(X_isomap[0,:],X_isomap[1,:],'o', markersize=12, color='m', label='Isomap')
plt.plot(X_svd[0,:],X_svd[1,:],'o', markersize=12, color='r', label='SVD')
plt.plot(X_mds[0,:],X_mds[1,:],'o', markersize=12, color='g', label='MDS')


plt.title('Singular Values from SVD / MDS / Isomap')
plt.legend()

plt.show()


# # Robust PCA from Prof. Paffenroth

# In[30]:

# import sys
# sys.path.append('C:/workspace/nldr')
#from numba import jit
from playground.src.lib.shrink import shrink
from eRPCAviaADMMFast import eRPCA


# In[31]:

#@jit
def test_erpca(M):
    M = np.matrix(M)
    m = M.shape[0]
    n = M.shape[1]
    obs = m*100
    print('m',m,'n',n,'obs',obs)
    
    u = [] # np.empty((m, 1), dtype=np.float32) # []
    v = [] # np.empty((n, 1), dtype=np.float32) #  []
    vecM = [] # np.empty((obs, 1), dtype=np.float32) #
    vecEpsilon = [] # np.empty((obs, 1), dtype=np.float32) #

    print('starting to generate data')

    # Note, this code can create duplicates, but that is ok
    # for  coo_matrix
    for k in range(obs):
        i = np.random.randint(m)
        j = np.random.randint(n)
        u.append(i)
        v.append(j)
        vecEpsilon.append(1e-3)
        Mij = M[i,j]
        vecM.append(Mij)

    u = np.array(u)
    v = np.array(v)
    vecM = np.array(vecM)
    vecEpsilon = np.array(vecEpsilon)

    print('total number of entries: ',m*n)
    print('observed entries:',len(u))

    print('starting solve')
    # I made maxRank a bit bigger to make the convergence more uniform, and made
    # lam larger, to keep things out of S.
    lam = 1./np.sqrt(np.max([m, n]))
    [U, E, VT, S] = eRPCA(m, n, u, v, vecM, vecEpsilon, maxRank=50, lam=lam, rho=None, mu=None, maxIteration=100)

    print(np.sum(E) + lam*np.linalg.norm(shrink(vecEpsilon, S.data),1))
    
    L = U*np.diag(E)*VT
    S = S.todense()
    Err = M-(L+S)

    epsilon = 0.1 #for plotting only
    truncateK = 3
    
    fig = plt.figure(figsize=(20,20))
    f,ax = plt.subplots(ncols=2,nrows=2,figsize=(16,16))
    ax[0,0].semilogy(E)
    ax[0,0].set_title('Singular values')

    ax[0,1].plot(np.array(Err).flatten().T)
    ax[0,1].axhline(epsilon,color='r')
    ax[0,1].axhline(-epsilon,color='r')
    ax[0,1].set_title('Err')

    ax[1,0].imshow(S)
    ax[1,0].set_title('S as an image')

    ax[1,1].plot(S.flatten().T)
    ax[1,1].set_title('S as a plot')
    f.savefig('sparse_detection_%5.2f_%d.svg'%(epsilon,truncateK))

    return U, E, VT, S, Err


# In[32]:

# I do the correlation matrix, since it is smaller and the code is then faster.
[U, E, VT, S, Err] = test_erpca(frame.T.corr())


# # To install please run "pip install mpld3"

# In[ ]:




# In[34]:

import matplotlib.pyplot as plt
import numpy as np
import mpld3

fig, ax = plt.subplots(subplot_kw=dict(axisbg='#EEEEEE'), figsize=(10,10))

N = Xh.shape[0]
X = Xh[0,:]
Y = Xh[1,:]

scatter = ax.scatter(X,
                     Y,
                     c=np.random.random(size=N),
                     s=1000 * np.random.random(size=N),
                     alpha=0.3,
                     cmap=plt.cm.jet)
ax.grid(color='white', linestyle='solid')

ax.set_title("Return Scatter Plot", size=20)

labels = [ticker.Symbol for i, ticker in tickers.iterrows()]
tooltip = mpld3.plugins.PointLabelTooltip(scatter, labels=labels)
mpld3.plugins.connect(fig, tooltip)

mpld3.enable_notebook()


# In[ ]:

# %matplotlib inline

# S = np.diag(s)
# # np.allclose(frame, np.dot(U, np.dot(S, V)))

# reduced_frame = np.dot(U, np.dot(S, V))
# plt.figure(figsize=(20,10))
# plt.plot(reduced_frame.T)
# plt.show()


