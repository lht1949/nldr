import pandas as pd

def get_data(filename="data60min.csv", **kwargs):
	df60min = pd.read_csv("data60min.csv")

	df_cols = df60min.columns[1:]
	df_idx = df60min.ix[:, 0]
	df_data = df60min.ix[0:, 1:]

	# df60 = pd.DataFrame(df_data.values, index=tickers.ix[:, 0].values, columns=df_cols.values)
	df60 = pd.DataFrame(df_data.values, index=df_idx.values, columns=df_cols.values)

	df60_ret = np.log(df60.T / df60.T.shift())
	df = df60_ret.T
	df.drop('201601040900', axis=1, inplace=True)
	return df