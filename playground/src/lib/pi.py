#! /usr/bin/python

def Pi(Omega, X):
    """ The projection operator.
        This implementation is intentionally slow but transparent as
        to the mathematics.

        Parameters
        ----------
        Omega - a matrix of elements that evaluate to True and False and we
                project onto the True elements (and False elements are 
                0 in the output matrix).
        X - the matrix to project

        Output
        ------
        The projected matrix
    """
    for i in range(X.shape[0]):
        for j in range(X.shape[1]):
            if not Omega[i,j]:
                X[i,j] = 0
    return X
