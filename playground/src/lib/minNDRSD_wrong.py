#! /usr/bin/env python

import numpy as np
import scipy.weave as wv
from scipy.weave import converters
from playground.src.lib.shrink import shrink


def minNDRSD(A, Y1, Y2, E, mu, hasWeave=True):
    """Compute a fast minimization of shrinkage plus Frobenius norm.

    The is computes the minium of the following objective.

    .. math::

        \langle Y_1, \mathcal{S}_{\boldsymbol \epsilon}( P_{\Omega} ( S_D ) ) \rangle + \\
        & \frac{\mu}{2}\| \mathcal{S}_{\boldsymbol \epsilon}( P_{\Omega} ( S_D ) ) \|_F^2 + \\
        & \langle Y_2, P_{\Omega}(S_D) \rangle + \\
        & \frac{\mu}{2}\| P_{\Omega}(M_D) - ( P_{\Omega}(D(L_G)) + P_{\Omega}(S_D)) \|_F^2 

    Args:
        A: A numpy array.

        Y1, Y2: Numpy arrays of Lagrange multipliers.

        E: A numpy array of error bounds.

        mu: The value of :math:`\mu`.

    Returns:
        The value of :math:`S` that achieves the minimum.
    """
    assert len(A.shape) == 1, 'A can only be a vector'
    assert A.shape == E.shape, 'A and E have  to have the same size'
    assert A.shape == Y1.shape, 'A and Y1 have  to have the same size'
    assert A.shape == Y2.shape, 'A and Y2 have  to have the same size'
    # Note, while the derivative is always zero when you use the
    # formula below, it is only a minimum if the second derivative is
    # positive.  The second derivative happens positive if and only
    # \mu is positive.
    assert mu >= 0., 'mu must be >= 0'

    S = np.zeros(A.shape)
    if hasWeave:
        AArray = np.array(A)
        Y1Array = np.array(Y1)
        Y2Array = np.array(Y2)
        EArray = np.array(E)
        k = len(A)
        mu = float(mu)
        code = """
        for(int i=0;i<k;i++) {
            if( (1/(2*mu))*(-Y1Array(i) - Y2Array(i) - mu*EArray(i) + mu*AArray(i) ) < -EArray(i) ) {
                S(i) = (1/(2*mu))*(-Y1Array(i) - Y2Array(i) - mu*EArray(i) + mu*AArray(i) ); }
            else if( (-EArray(i) < (1/mu)*(- Y2Array(i) + mu*AArray(i))) && 
                     ((1/mu)*(- Y2Array(i) + mu*AArray(i)) < EArray(i)) ) {
                S(i) = (1/mu)*(- Y2Array(i) + mu*AArray(i)); }
            else if( EArray(i) <  (1/(2*mu))*(-Y1Array(i) - Y2Array(i) + mu*EArray(i) + mu*AArray(i) ) ) {
                S(i) = (1/(2*mu))*(-Y1Array(i) - Y2Array(i) + mu*EArray(i) + mu*AArray(i) ); }
            else {
                double Sp = Y2Array(i)*EArray(i) + (mu/2)*(AArray(i)-EArray(i))*(AArray(i)-EArray(i));
                double Sm = -Y2Array(i)*EArray(i) + (mu/2)*(AArray(i)+EArray(i))*(AArray(i)+EArray(i));
                if(Sp < Sm) {
                    S(i) = EArray(i); }
                else {
                    S(i) = -EArray(i); }
            }
        }
        """
        wv.inline(code,
                  ['AArray', 'Y1Array', 'Y2Array', 'EArray',
                   'S', 'k', 'mu'],
                  type_converters=converters.blitz)
    else:
        for i in range(len(A)):
            if ((1/(2*mu))*(-Y1[i] - Y2[i] - mu*E[i] + mu*A[i]) < -E[i]):
                S[i] = (1/(2*mu))*(-Y1[i] - Y2[i] - mu*E[i] + mu*A[i])
            elif ((-E[i] < (1/mu)*(-Y2[i] + mu*A[i])) and
                  ((1/mu)*(-Y2[i] + mu*A[i]) < E[i])):
                S[i] = ((1/mu)*(- Y2[i] + mu*A[i]))
            elif E[i] < (1/(2*mu))*(-Y1[i] - Y2[i] + mu*E[i] + mu*A[i]):
                S[i] = (1/(2*mu))*(-Y1[i] - Y2[i] + mu*E[i] + mu*A[i])
            else:
                Sp = Y2[i]*E[i] + (mu/2)*(A[i]-E[i])*(A[i]-E[i])
                Sm = -Y2[i]*E[i] + (mu/2)*(A[i]+E[i])*(A[i]+E[i])
                if Sp < Sm:
                    S[i] = E[i]
                else:
                    S[i] = -E[i]
    return S


def objective(S, A, Y1, Y2, E, mu):
    temp1 = np.dot(Y1, shrink(E, S))
    temp2 = (mu/2.)*(np.linalg.norm(shrink(E, S))**2)
    temp3 = np.dot(Y2, S)
    temp4 = (mu/2.)*(np.linalg.norm(A-S)**2)
    return temp1+temp2+temp3+temp4


def plot_objective():
    A = np.array([ 0.     ,  0.15854,  0.71644,  5.     ,  5.     ,  0.15854,
        0.     ,  0.20875,  1.19954,  5.     ,  0.71644,  0.20875,
        0.     ,  0.31242,  5.     ,  5.     ,  1.19954,  0.31242,
        0.     ,  0.47801,  5.     ,  5.     ,  5.     ,  0.47801,  0.     ])
    Y1 = np.array([ 0.     ,  0.00741,  0.02558,  0.     ,  0.     ,  0.00741,
        0.     ,  0.00957,  0.02849,  0.     ,  0.02558,  0.00957,
        0.     ,  0.01373,  0.     ,  0.     ,  0.02849,  0.01373,
        0.     ,  0.01947,  0.     ,  0.     ,  0.     ,  0.01947,  0.     ])
    Y2 = np.array([ 0.     ,  0.00784,  0.03542,  0.     ,  0.     ,  0.00784,
        0.     ,  0.01032,  0.05931,  0.     ,  0.03542,  0.01032,
        0.     ,  0.01545,  0.     ,  0.     ,  0.05931,  0.01545,
        0.     ,  0.02363,  0.     ,  0.     ,  0.     ,  0.02363,  0.     ])
    E = np.array([  0.00000e+00,   4.32591e-03,   9.95856e-02,   5.00000e+00,
         5.00000e+00,   4.32591e-03,   0.00000e+00,   7.57776e-03,
         3.11687e-01,   5.00000e+00,   9.95856e-02,   7.57776e-03,
         0.00000e+00,   1.73463e-02,   5.00000e+00,   5.00000e+00,
         3.11687e-01,   1.73463e-02,   0.00000e+00,   4.20675e-02,
         5.00000e+00,   5.00000e+00,   5.00000e+00,   4.20675e-02,
         0.00000e+00])
    mu = 0.304172303889

    #A = np.random.random(size=A.shape)
    #Y1 = np.random.random(size=A.shape)
    #Y2 = np.random.random(size=A.shape)
    E = np.ones(A.shape)*1e-4
    #mu = 0.1

    print
    print 'A, Y1, Y2, E, mu'
    print A, Y1, Y2, E, mu
    Smin = minNDRSD(A, Y1, Y2, E, mu, hasWeave=True)
    print 'Smin'
    print Smin
    SminObj = objective(Smin, A, Y1, Y2, E, mu)
    print 'Should be smallest', SminObj

    X = []
    Y = []
    k = 1
    # perturb = np.random.random(size=A.shape)
    perturb = np.zeros(A.shape)

    for s in np.linspace(Smin[k]-(1e-1), Smin[k]+(1e-1), 100):
        # This should be smaller that E, otherwise the objective
        # is flat.
        perturb[k] = s
        pObj = objective(Smin+perturb, A, Y1, Y2, E, mu)
        X.append(s)
        Y.append(pObj)

    import matplotlib.pylab as py
    py.figure(1)
    py.plot(X, Y)
    py.show()


def test_minNDRSD():
    np.random.seed(1234)
    A = np.random.random(size=[5])
    Y1 = np.random.random(size=[5])
    Y2 = np.random.random(size=[5])
    E = np.ones([5])*1e-4
    # Needs to be bigger than 0.
    mu = 0.1

    print
    print 'A, Y1, Y2, E, mu'
    print A, Y1, Y2, E, mu
    Smin = minNDRSD(A, Y1, Y2, E, mu, hasWeave=True)
    print 'Smin'
    print Smin
    SminObj = objective(Smin, A, Y1, Y2, E, mu)
    print 'Should be smallest', SminObj

    for i in range(5):
        # This should be smaller that E, otherwise the objective
        # is flat.
        perturb = np.random.random(size=A.shape)*1e-3
        pObj = objective(Smin+perturb, A, Y1, Y2, E, mu)
        print pObj
        assert SminObj <= pObj


def test_minNDRSD2():
    A = np.array([ 0.     ,  0.15854,  0.71644,  5.     ,  5.     ,  0.15854,
        0.     ,  0.20875,  1.19954,  5.     ,  0.71644,  0.20875,
        0.     ,  0.31242,  5.     ,  5.     ,  1.19954,  0.31242,
        0.     ,  0.47801,  5.     ,  5.     ,  5.     ,  0.47801,  0.     ])
    Y1 = np.array([ 0.     ,  0.00741,  0.02558,  0.     ,  0.     ,  0.00741,
        0.     ,  0.00957,  0.02849,  0.     ,  0.02558,  0.00957,
        0.     ,  0.01373,  0.     ,  0.     ,  0.02849,  0.01373,
        0.     ,  0.01947,  0.     ,  0.     ,  0.     ,  0.01947,  0.     ])
    Y2 = np.array([ 0.     ,  0.00784,  0.03542,  0.     ,  0.     ,  0.00784,
        0.     ,  0.01032,  0.05931,  0.     ,  0.03542,  0.01032,
        0.     ,  0.01545,  0.     ,  0.     ,  0.05931,  0.01545,
        0.     ,  0.02363,  0.     ,  0.     ,  0.     ,  0.02363,  0.     ])
    E = np.array([  0.00000e+00,   4.32591e-03,   9.95856e-02,   5.00000e+00,
         5.00000e+00,   4.32591e-03,   0.00000e+00,   7.57776e-03,
         3.11687e-01,   5.00000e+00,   9.95856e-02,   7.57776e-03,
         0.00000e+00,   1.73463e-02,   5.00000e+00,   5.00000e+00,
         3.11687e-01,   1.73463e-02,   0.00000e+00,   4.20675e-02,
         5.00000e+00,   5.00000e+00,   5.00000e+00,   4.20675e-02,
         0.00000e+00])
    mu = 0.304172303889

    print
    print 'A, Y1, Y2, E, mu'
    print A, Y1, Y2, E, mu
    Smin = minNDRSD(A, Y1, Y2, E, mu, hasWeave=True)
    print 'Smin'
    print Smin
    SminObj = objective(Smin, A, Y1, Y2, E, mu)
    print 'Should be smallest', SminObj

    for i in range(5):
        # This should be smaller that E, otherwise the objective
        # is flat.
        perturb = np.random.random(size=A.shape)*1e-3
        pObj = objective(Smin+perturb, A, Y1, Y2, E, mu)
        print pObj
        assert SminObj <= pObj

if __name__ == '__main__':
    test_minNDRSD()
    test_minNDRSD2()
    # plot_objective()
